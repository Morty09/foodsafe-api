package com.esgi.foodsafeapi.models;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.List;

@Document( collection = "ingredients" )
@Entity
public class Ingredient implements Serializable {

    @Id
    public String _id;

    public String ingredientName;
    @Column
    @ElementCollection()
    public List<String> productsIds;
    @Column
    @ElementCollection()
    public List<String> ingredientKeyName;
    public Boolean allergenType;

    public Ingredient() {}

    public Ingredient(String ingredientName, List<String> productsIds, List<String> ingredientKeyName, Boolean allergenType) {
        this.ingredientName = ingredientName;
        this.productsIds = productsIds;
        this.ingredientKeyName = ingredientKeyName;
        this.allergenType = allergenType;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getIngredientName() {
        return ingredientName;
    }

    public void setIngredientName(String ingredientName) {
        this.ingredientName = ingredientName;
    }

    public List<String> getProductsIds() {
        return productsIds;
    }

    public void setProductsIds(List<String> productsIds) {
        this.productsIds = productsIds;
    }

    public List<String> getIngredientKeyName() {
        return ingredientKeyName;
    }

    public void setIngredientKeyName(List<String> ingredientKeyName) {
        this.ingredientKeyName = ingredientKeyName;
    }

    public Boolean getAllergenType() {
        return allergenType;
    }

    public void setAllergenType(Boolean allergenType) {
        this.allergenType = allergenType;
    }

    @Override
    public String toString() {
        return "Ingredient{" +
                "_id='" + _id + '\'' +
                ", ingredientName='" + ingredientName + '\'' +
                ", productsIds=" + productsIds +
                ", ingredientKeyName='" + ingredientKeyName + '\'' +
                ", allergenType=" + allergenType +
                '}';
    }
}
