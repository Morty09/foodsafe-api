package com.esgi.foodsafeapi.utils;

import com.esgi.foodsafeapi.Service.ProductService;
import com.esgi.foodsafeapi.models.Product;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.*;

@EnableBatchProcessing
@Configuration
public class BatchUpdate extends UtilsProduct {

    @Autowired
    private ProductService productService;

    @Bean
    public Boolean feelDatabase() {
        /*try {

            List<Product> products = productService.getAll();
            updateAllProducts(products);

        } catch (Exception e) {
            throw e;
        }*/

        return true;
    }

    public void updateAllProducts(List<Product> productsFromDB) {

        for (Product p : productsFromDB) {
            Product product = getOffProduct(p.code);
            productService.update(p.code, product);
        }
    }

}
