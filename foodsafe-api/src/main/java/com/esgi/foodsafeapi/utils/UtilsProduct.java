package com.esgi.foodsafeapi.utils;

import com.esgi.foodsafeapi.models.Product;
import com.google.gson.JsonArray;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.transaction.Transactional;
import java.util.*;

import static org.hibernate.internal.util.collections.ArrayHelper.toList;

public class UtilsProduct {

    public Product getOffProduct(String code) {

        String uri = "https://fr.openfoodfacts.org/api/v0/produit/" + code + ".json";

        try {
            RestTemplate restTemplate = new RestTemplate();
            String result = restTemplate.getForObject(uri, String.class);

            JsonObject product = new JsonParser().parse(result).getAsJsonObject();

            if (product.get("status").getAsInt() == 0) {
                return null;
            }

            product = product.get("product").getAsJsonObject();

            JsonArray jArray = product.getAsJsonArray("ingredients");
            List<Map> listIngredients = ingredientListFormatter(jArray);

            String stores = String.valueOf(product.get("stores")).replaceAll("\"", "");
            String product_name = String.valueOf(product.get("product_name")).replaceAll("\"", "");

            Map<String, Object> mapNutrient_levels = transformToMap(product.get("nutrient_levels").getAsJsonObject());

            Map<String, Object> mapNutriment = transformToMap(product.get("nutriments").getAsJsonObject());

            String quantity = String.valueOf(product.get("quantity")).replaceAll("\"", "");
            String ingredients_text = String.valueOf(product.get("ingredients_text")).replaceAll("\"", "");
            String image_url = String.valueOf(product.get("image_url")).replaceAll("\"", "");

            JsonArray tracesArray = product.getAsJsonArray("traces_tags");
            List<String> traces_tags = transformToStringList(tracesArray);

            JsonArray vitaminsArray = product.getAsJsonArray("vitamins_tags");
            List<String> vitamins_tags = transformToStringList(vitaminsArray);

            String categories = String.valueOf(product.get("categories")).replaceAll("\"", "");
            String origins = String.valueOf(product.get("origins")).replaceAll("\"", "");
            String codeProduct = String.valueOf(product.get("code")).replaceAll("\"", "");
            String allergens = String.valueOf(product.get("allergens")).replaceAll("\"", "");
            String nutrition_grades = String.valueOf(product.get("nutrition_grades")).replaceAll("\"", "");
            String ingredients_text_with_allergens = String.valueOf(product.get("ingredients_text_with_allergens")).replaceAll("\"", "");
            String labels = String.valueOf(product.get("labels")).replaceAll("\"", "");
            String brands = String.valueOf(product.get("brands")).replaceAll("\"", "");

            JsonArray allergensArray = product.getAsJsonArray("allergens_tags");
            List<String> allergens_tags = transformToStringList(allergensArray);

            String traces = String.valueOf(product.get("traces")).replaceAll("\"", "");

            Product p = new Product(listIngredients, stores, product_name, mapNutrient_levels, mapNutriment, quantity, ingredients_text, image_url, traces_tags, vitamins_tags, categories,
                    origins, codeProduct, allergens, nutrition_grades, ingredients_text_with_allergens, labels, brands, allergens_tags, traces);

            return p;
        } catch (Exception e) {
            throw e;
        }

    }

    public List<String> transformToStringList(JsonArray jsonArray) {

        List<String> list = new ArrayList<>();
        if (jsonArray != null) {
            for (int i = 0; i < jsonArray.size(); i++) {
                list.add(jsonArray.get(i).toString().replaceAll("\"", ""));
            }
        }
        return list;
    }

    public Map<String, Object> transformToMap(JsonObject jsonObject) {

        Map<String, Object> map = new HashMap<String, Object>();

        try {
            JSONObject objectToTransform = new JSONObject(jsonObject.toString());
            map = toMap(objectToTransform);
        } catch (JSONException e){
            e.printStackTrace();
        }

        return map;
    }

    public Map<String, Object> toMap(JSONObject object) throws JsonIOException {
        Map<String, Object> map = new HashMap<String, Object>();

        Iterator<String> keysItr = object.keys();
        while(keysItr.hasNext()) {
            String key = keysItr.next();
            Object value = null;
            try {
                value = object.get(key);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            if(value instanceof JSONArray) {
                value = toList((JSONArray) value);
            }

            else if(value instanceof JSONObject) {
                value = toMap((JSONObject) value);
            }
            map.put(key, value);
        }
        return map;
    }

    public List<Map> ingredientListFormatter(JsonArray jArray) {

        List<Map> listIngredients = new ArrayList<>();
        Map<String, Object> map = new HashMap<String, Object>();

        if (jArray != null) {
            for (int i = 0; i < jArray.size(); i ++) {
                JsonObject ingredientsObject = jArray.get(i).getAsJsonObject();
                map = transformToMap(ingredientsObject);
                listIngredients.add(map);
            }
        }

        return listIngredients;
    }

}
