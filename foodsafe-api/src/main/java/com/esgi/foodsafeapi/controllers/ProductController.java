package com.esgi.foodsafeapi.controllers;

import com.esgi.foodsafeapi.Exceptions.ResourceNotFoundException;
import com.esgi.foodsafeapi.Service.IngredientService;
import com.esgi.foodsafeapi.Service.ProductService;
import com.esgi.foodsafeapi.interfaces.IngredientsRepository;
import com.esgi.foodsafeapi.interfaces.ProductRepository;
import com.esgi.foodsafeapi.models.Ingredient;
import com.esgi.foodsafeapi.models.Product;
import com.esgi.foodsafeapi.utils.UtilsProduct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import org.springframework.web.bind.annotation.*;
import java.util.*;

@RestController
public class ProductController extends UtilsProduct {

    @Autowired
    ProductRepository productRepository;

    @Autowired
    IngredientService ingredientService;

    @Autowired
    IngredientsRepository ingredientsRepository;

    @Autowired
    ProductService productService;

    public ProductController() {}

    @RequestMapping(method=RequestMethod.POST, value="/products/add")
    public ResponseEntity<String> postProduct(@RequestBody Product product) {
        String id = productService.post(product)._id;
        return new ResponseEntity<>(id, HttpStatus.CREATED);
    }

    @RequestMapping(method= RequestMethod.GET, value="/products")
    public ResponseEntity<List<Product>> getAllProducts() {
        return new ResponseEntity<>(productService.getAll(), HttpStatus.FOUND);
    }

    @RequestMapping(method=RequestMethod.GET, value="/products/{id}")
    public ResponseEntity<Product> getProductById(@PathVariable String id) {

        Product p = productService.getById(id).orElseThrow(() -> new ResourceNotFoundException("Product", "id", id));
        return new ResponseEntity<>(p, HttpStatus.FOUND);

    }

    @RequestMapping(method=RequestMethod.PUT, value="/products/{code}")
    public ResponseEntity<Product> updateProduct(@PathVariable String code, @RequestBody Product product) {

        Product newProduct = productService.update(code, product).orElseThrow(() -> new ResourceNotFoundException("Product", "code", code));
        return new ResponseEntity<>(newProduct, HttpStatus.OK);

    }

    @RequestMapping( method = RequestMethod.DELETE, value = "/products/{id}")
    public ResponseEntity<Object> deleteProduct(@PathVariable String id) {

        Product productToDelete = productService.getById(id).orElseThrow(() -> new ResourceNotFoundException("Product", "id", id));
        productService.delete(productToDelete);
        return new ResponseEntity<>("Product with id: " + id + " was deleted" +
                " successfully", HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/products/code/{code}")
    public ResponseEntity<Object> getProductByCode(@PathVariable String code) {

        Product p = productService.getByCode(code).isPresent() ? productService.getByCode(code).get() : null;
        Product pOff = new Product();
        if (p == null) {
            System.out.println("NOT IS OUR BASE");
            if (getOffProduct(code) == null) {
                return new ResponseEntity<>(new ResourceNotFoundException("Product", "code", code, "404"), HttpStatus.NOT_FOUND);
            }
            pOff = getOffProduct(code);
            postProduct(pOff);
            createIngredients(pOff.code, pOff.ingredients);
        } else
            pOff = p;
        return new ResponseEntity<>(pOff, HttpStatus.FOUND);

    }

    public void createIngredients(String p_code, List<Map> ingredients) {

        for (int i = 0; i < ingredients.size(); i++) {

            Ingredient newIngredient = new Ingredient();
            Map<String, Object> currentIngredient = ingredients.get(i);

            newIngredient.setIngredientName(currentIngredient.get("text").toString());
            newIngredient.set_id(currentIngredient.get("id").toString());

            if (ingredientService.getById(newIngredient._id).isPresent()) {
                updateIngredientIfExist(newIngredient, p_code);
            } else {
                List<String> productIds = new ArrayList<>();
                productIds.add(p_code);
                newIngredient.setProductsIds(productIds);
                ingredientService.post(newIngredient);
            }
        }
    }

    public void updateIngredientIfExist(Ingredient ingredientToUpdate, String p_code) {

        Optional<Ingredient> existingIngredient = ingredientService.getById(ingredientToUpdate._id);
        List<String> productsIds = existingIngredient.get().getProductsIds();
        boolean exist = false;
        int i = 0;

        if (productsIds.isEmpty()) {
            productsIds.add(p_code);
        } else {
            while (!exist && i < productsIds.size()) {
                if (productsIds.get(i).equals(p_code)) {
                    exist = true;
                    break;
                }
                i++;
            }

            if (!exist)
                productsIds.add(p_code);
        }
        ingredientToUpdate.setProductsIds(productsIds);
        ingredientService.update(ingredientToUpdate._id, ingredientToUpdate);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/products/ingredient/{ingredient}")
    public ResponseEntity<List<Product>> getProductByIngredients(@PathVariable String ingredient) {

        List<Product> productList = new ArrayList<>();
        Ingredient ingredientSearched = ingredientService.getByName(ingredient).orElseThrow(() -> new ResourceNotFoundException("Ingredient", "name", ingredient));
        List<String> productsCodes = ingredientSearched.getProductsIds();

        for (String p_code : productsCodes) {
            productList.add(productService.getByCode(p_code).get());
        }

        return new ResponseEntity<>(productList, HttpStatus.FOUND);

    }

    @RequestMapping(method = RequestMethod.GET, value = "/products/traces/{code}")
    public ResponseEntity<List<String>> getTracesOfProducts(@PathVariable String code) {

        Product product = productService.getByCode(code).orElseThrow(() -> new ResourceNotFoundException("Product", "code", code));
        String traces = product.getTraces();
        List<String> tracesList = new ArrayList<>();
        tracesList = splitStringToGetWord(traces);

        for (String s : tracesList) {
            System.out.println(s);
        }

        return new ResponseEntity<>(tracesList, HttpStatus.FOUND);

    }

    @RequestMapping(method = RequestMethod.GET, value = "/products/allergens/{code}")
    public ResponseEntity<List<String>> getAllergensOfProducts(@PathVariable String code) {

        Product product = productService.getByCode(code).orElseThrow(() -> new ResourceNotFoundException("Product", "code", code));
        String allergens = product.getAllergens();
        List<String> allergensList = new ArrayList<>();

        allergensList = splitStringToGetWord(allergens);

        for (String s : allergensList) {
            System.out.println(s);
        }

        return new ResponseEntity<>(allergensList, HttpStatus.FOUND);

    }

    public List<String> splitStringToGetWord(String str) {

        List<String> stringList = new ArrayList<>();
        int counter = 0;

        for (int i = 0; i <= str.length() - 1; i++) {

            if (str.charAt(i) == ',') {
                String word = str.substring(counter, i);
                stringList.add(word);
                counter+= word.length() + 1;
            } else if (i == str.length() - 1) {
                String word = str.substring(counter, i + 1);
                stringList.add(word);
            }
        }

        return stringList;

    }

}
