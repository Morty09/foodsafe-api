package com.esgi.foodsafeapi.interfaces;

import com.esgi.foodsafeapi.models.Product;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public interface ProductRepository extends MongoRepository<Product, String> {

    Optional<Product> findByCode(String code);

}