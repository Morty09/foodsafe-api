import pymongo

myclient = pymongo.MongoClient("mongodb://localhost:27017/")
mydb = myclient["foodsafedb"]
mycol = mydb["products_test"]

#mydict = { "name": "John", "address": "Highway 37" }

data = {
    "ingredients":[
        {
            "rank":1,
            "text":"Chocolat au lait",
            "id":"en:milk-chocolate",
            "percent":"89"
        },
        {
            "rank":2,
            "text":"_beurre_ concentré",
            "id":"en:concentrated-butter"
        },
        {
            "rank":3,
            "id":"en:cocoa-powder",
            "text":"poudre de cacao"
        },
        {
            "text":"sucre",
            "id":"en:sugar"
        },
        {
            "id":"en:cocoa-butter",
            "text":"beurre de cacao"
        },
        {
            "text":"poudre de _lait_ entier",
            "id":"en:whole-milk-powder"
        },
        {
            "text":"pâte de cacao",
            "id":"en:cocoa-paste"
        },
        {
            "id":"en:natural-vanilla-flavouring",
            "text":"arôme naturel de vanille"
        },
        {
            "id":"en:emulsifier",
            "text":"émulsifiant"
        },
        {
            "id":"en:soya-lecithin",
            "text":"lécithines de _soja_"
        }],
        "stores":"Monoprix",
        "product_name":"Truffes au Chocolat au Lait",
        "nutrient_levels":{
            "fat_100g":"43",
            "proteins_unit":"g",
            "energy_unit":"kJ",
            "proteins_value":"6.2",
            "carbohydrates_100g":"45",
            "saturated-fat_value":"27",
            "saturated-fat":"27",
            "salt_value":"0.18",
            "proteins_serving":"",
            "energy_100g":"2477",
            "proteins_100g":6.2,
            "proteins":6.2,
            "fat":"43",
            "salt_unit":"g",
            "sodium_value":"0.07086614173228345",
            "saturated-fat_unit":"g",
            "sugars_unit":"g",
            "nutrition-score-fr":"26",
            "energy":"2477",
            "sodium_unit":"g",
            "saturated-fat_serving":"",
            "sugars_serving":"",
            "carbohydrates":"45",
            "sugars":"45",
            "salt":0.18,
            "sugars_100g":"45",
            "salt_serving":"",
            "sugars_value":"45",
            "energy_serving":"",
            "nutrition-score-uk":"26",
            "nova-group_serving":"4",
            "nutrition-score-fr_100g":"26",
            "carbohydrates_serving":"",
            "sodium_100g":0.0708661417322835,
            "salt_100g":0.18,
            "sodium_serving":"",
            "saturated-fat_100g":"27",
            "fat_serving":"",
            "sodium":0.0708661417322835,
            "nova-group":"4",
            "nutrition-score-uk_100g":"26",
            "fat_unit":"g",
            "energy_value":"2477",
            "fat_value":"43",
            "nova-group_100g":"4",
            "carbohydrates_value":"45",
            "carbohydrates_unit":"g"
            },
            "nutriments":{
                "fat_100g":"43",
                "proteins_unit":"g",
                "energy_unit":"kJ",
                "proteins_value":"6.2",
                "carbohydrates_100g":"45",
                "saturated-fat_value":"27",
                "saturated-fat":"27",
                "proteins_serving":"",
                "salt_value":"0.18",
                "energy_100g":"2477",
                "proteins_100g":6.2,
                "proteins":6.2,
                "fat":"43",
                "sodium_value":"0.07086614173228345",
                "salt_unit":"g",
                "sugars_unit":"g",
                "saturated-fat_unit":"g",
                "nutrition-score-fr":"26",
                "energy":"2477",
                "sodium_unit":"g",
                "saturated-fat_serving":"",
                "sugars_serving":"",
                "carbohydrates":"45",
                "sugars":"45",
                "salt":0.18,
                "sugars_100g":"45",
                "salt_serving":"",
                "sugars_value":"45",
                "energy_serving":"",
                "nutrition-score-uk":"26",
                "nova-group_serving":"4",
                "nutrition-score-fr_100g":"26",
                "carbohydrates_serving":"",
                "sodium_100g":0.0708661417322835,
                "salt_100g":0.18,
                "saturated-fat_100g":"27",
                "sodium_serving":"",
                "fat_serving":"",
                "sodium":0.0708661417322835,
                "nutrition-score-uk_100g":"26",
                "fat_unit":"g",
                "nova-group":"4",
                "energy_value":"2477",
                "fat_value":"43",
                "nova-group_100g":"4",
                "carbohydrates_value":"45",
                "carbohydrates_unit":"g"
                },
                "quantity":"250 g",
                "ingredients_text":"Chocolat au lait 89 % (sucre, beurre de cacao, poudre de _lait_ entier, pâte de cacao, arôme naturel de vanille, émulsifiant : lécithines de _soja_), _beurre_ concentré, poudre de cacao.",
                "image_url":"https://static.openfoodfacts.org/images/products/335/003/343/7111/front_fr.10.400.jpg",
                "traces_tags":[
                    "en:eggs",
                    "en:gluten",
                    "en:nuts",
                    "en:peanuts",
                    "en:sesame-seeds"
                ],
                "vitamins_tags":[],
                "categories":"Snacks sucrés,Confiseries,Confiseries chocolatées,Bonbons de chocolat,Truffes en chocolat",
                "origins":"",
                "code":"3350033437111",
                "allergens":"en:milk,en:soybeans",
                "nutrition_grades":"e",
                "ingredients_text_with_allergens":"Chocolat au lait 89 % (sucre, beurre de cacao, poudre de <span class=\\allergen\\>lait</span> entier, pâte de cacao, arôme naturel de vanille, émulsifiant : lécithines de <span class=\\allergen\\>soja</span>), <span class=\\allergen\\>beurre</span> concentré, poudre de cacao.",
                "labels":"Point Vert,Triman",
                "brands":"Monoprix Gourmet,Monoprix",
                "allergens_tags":[
                    "en:milk",
                    "en:soybeans"
                ],
                "traces":"en:eggs,en:gluten,en:nuts,en:peanuts,en:sesame-seeds",
                "_class":"com.esgi.foodsafeapi.models.Product"
    }

#listProducts = list()
#for j in range(10):
#    listProducts.append(data)

#print(listProducts)

for i in range(1000000):
    data.pop('_id', None)
    mycol.insert_one(data)