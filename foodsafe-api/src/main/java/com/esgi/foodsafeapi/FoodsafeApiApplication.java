package com.esgi.foodsafeapi;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication
public class FoodsafeApiApplication {

	public static void main(String[] args) {

		SpringApplication.run(FoodsafeApiApplication.class, args);
	}

}
