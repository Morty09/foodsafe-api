package com.esgi.foodsafeapi.Service;

import com.esgi.foodsafeapi.interfaces.IngredientsRepository;
import com.esgi.foodsafeapi.models.Ingredient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.CriteriaBuilder;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class IngredientService {

    @Autowired
    public IngredientsRepository ingredientsRepository;

    public String post(Ingredient ingredient) {
        return ingredientsRepository.save(ingredient).get_id();
    }

    public List<Ingredient> getAll() {
        return ingredientsRepository.findAll();
    }

    public Optional<Ingredient> getById(String id) {
        return ingredientsRepository.findById(id);
    }

    public Optional<Ingredient> update(String id, Ingredient ingredient) {
        return ingredientsRepository.findById(id).map(ingredient2 -> {
                    if (ingredient.getProductsIds() != null)
                        ingredient2.setProductsIds(ingredient.productsIds);
                    if (ingredient.getAllergenType() != null)
                        ingredient2.setAllergenType(ingredient.allergenType);
                    if (ingredient.getIngredientKeyName() != null)
                        ingredient2.setIngredientKeyName(ingredient.ingredientKeyName);
                    if (ingredient.getIngredientName() != null)
                        ingredient2.setIngredientName(ingredient.ingredientName);
                    ingredientsRepository.save(ingredient2);
                    return ingredient2;
                }
        );
    }

    public Optional<Ingredient> getByName(String name) {
        return ingredientsRepository.findByIngredientName(name);
    }

    public void delete(Ingredient ingredient) {
        ingredientsRepository.delete(ingredient);
    }
}
