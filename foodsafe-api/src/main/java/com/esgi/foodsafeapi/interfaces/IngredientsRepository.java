

package com.esgi.foodsafeapi.interfaces;


import com.esgi.foodsafeapi.models.Ingredient;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.*;


@Repository
public interface IngredientsRepository extends MongoRepository<Ingredient, String> {

    Optional<Ingredient> findByIngredientName(String ingredientName);

    List<Ingredient> findByAllergenType(Boolean status);

}
