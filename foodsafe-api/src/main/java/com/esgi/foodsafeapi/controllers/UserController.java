package com.esgi.foodsafeapi.controllers;

import com.esgi.foodsafeapi.Exceptions.ResourceNotFoundException;
import com.esgi.foodsafeapi.Service.UserService;
import com.esgi.foodsafeapi.interfaces.UserRepository;
import com.esgi.foodsafeapi.models.Product;
import com.esgi.foodsafeapi.models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.xml.ws.Response;
import java.util.List;

@RestController
public class UserController {

    @Autowired
    UserRepository userRepository;

    @Autowired
    UserService userService;

    @RequestMapping(method=RequestMethod.GET, value="/users")
    public ResponseEntity<List<User>> getAllUsers() {
        return new ResponseEntity<>(userService.getAll(), HttpStatus.OK);
    }

    @RequestMapping(method=RequestMethod.POST, value="/users")
    public ResponseEntity<String> postUser(@RequestBody User user) {
        User usr = userService.post(user);
        return new ResponseEntity<>("User " + usr.getId()+ " was created successfully", HttpStatus.CREATED);
    }

    @RequestMapping(method=RequestMethod.GET, value="/users/{id}")
    public ResponseEntity<User> getUserById(@PathVariable String id) {
        User user = userService.getById(id).orElseThrow(() -> new ResourceNotFoundException("User", "id", id));
        return new ResponseEntity<>(user, HttpStatus.FOUND);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/users/lastname={lastname}")
    public ResponseEntity<List<User>> getUserByLastname(@PathVariable String lastname) {
        List<User> users = userService.getByLastName(lastname).orElseThrow(() -> new ResourceNotFoundException("User", "lastname", lastname));
        return new ResponseEntity<>(users, HttpStatus.FOUND);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/users/email={email}")
    public ResponseEntity<User> getUserByEmail(@PathVariable String email) {

        User user = userService.getByEmail(email).orElseThrow(() -> new ResourceNotFoundException("User", "email", email));
        return new ResponseEntity<>(user, HttpStatus.FOUND);

    }

    @RequestMapping(method = RequestMethod.GET, value = "/users/allergens/{id}")
    public ResponseEntity<List<String>> getAllergensOfUser(@PathVariable String id) {
        User user = userService.getById(id).orElseThrow(() -> new ResourceNotFoundException("User", "id", id));
        return new ResponseEntity<>(user.getList_allergen(), HttpStatus.FOUND);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/users/shop/{id}")
    public ResponseEntity<List<Product>> getShopListOfUser(@PathVariable String id) {
        User user = userService.getById(id).orElseThrow(() -> new ResourceNotFoundException("User", "id", id));
        return new ResponseEntity<>(user.getList_shop(), HttpStatus.FOUND);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/users/productHistory/{id}")
    public ResponseEntity<List<String>> getHistoryProductOfUser(@PathVariable String id) {
        User user = userService.getById(id).orElseThrow(() -> new ResourceNotFoundException("User", "id", id));
        return new ResponseEntity<>(user.getList_product_history(), HttpStatus.FOUND);
    }

    @RequestMapping(method=RequestMethod.PUT, value="/users/{id}")
    @ResponseStatus(value = HttpStatus.OK)
    public ResponseEntity<User> updateUser(@PathVariable String id, @RequestBody User user) {
        User newUser = userService.update(id, user).orElseThrow(() -> new ResourceNotFoundException("Ingredient", "id", id));
        return new ResponseEntity<>(newUser, HttpStatus.OK);
    }

    @RequestMapping(method= RequestMethod.DELETE, value="/users/{id}")
    @ResponseStatus(value = HttpStatus.OK)
    public ResponseEntity<String> delete(@PathVariable String id) {
        User userToDelete = userService.getById(id).orElseThrow(() -> new ResourceNotFoundException("User", "id", id));
        userRepository.delete(userToDelete);
        return new ResponseEntity<>("User with id " + userToDelete.getId() + " was deleted successfully", HttpStatus.OK);
    }

}
