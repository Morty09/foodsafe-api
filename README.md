# Foodsafe API

This API fetch the data of the open source database "Open Food Facts" 
It is created to feed the mobile application "Foodsafe"

* Realized with Java Spring Boot
* Includes batch processing to update our database with the datas of Open Food Facts
* Includes CSV parser (because the Open Food Facts datas are in CSV format)

## Purpose

Project realized in the context of school.

The purpose was to learn to handle a big amount of datas.

The use of an open source database was required

## Technologies

* Java
* SpringBoot
* Python

