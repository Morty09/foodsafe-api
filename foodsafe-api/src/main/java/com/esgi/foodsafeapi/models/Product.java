package com.esgi.foodsafeapi.models;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.google.gson.JsonObject;
import lombok.Data;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Document(collection = "myProducts")
//@Document(collection = "products_test")
@Data
public class Product implements ItemReader<Product> {

    @Id
    public String _id;

    @Column
    @ElementCollection()
    public List<Map> ingredients;
    public String stores;
    public String product_name;
    @Column
    @ElementCollection()
    public Map<String, Object> nutrient_levels;
    @Column
    @ElementCollection()
    public Map<String, Object> nutriments;
    public String quantity;
    public String ingredients_text;
    public String image_url;
    @Column
    @ElementCollection()
    public List<String> traces_tags;
    @Column
    @ElementCollection()
    public List<String> vitamins_tags;
    public String categories;
    public String origins;
    public String code;
    public String allergens;
    public String nutrition_grades;
    public String ingredients_text_with_allergens;
    public String labels;
    public String brands;
    @Column
    @ElementCollection()
    public List<String> allergens_tags;
    public String traces;

    public Product() {}

    public Product(List<Map> ingredients, String stores, String product_name, Map<String, Object> nutrient_levels, Map<String, Object> nutriments, String quantity, String ingredients_text, String image_url, List<String> traces_tags, List<String> vitamins_tags, String categories, String origins, String code, String allergens, String nutrition_grades, String ingredients_text_with_allergens, String labels, String brands, List<String> allergens_tags, String traces) {
        this.ingredients = ingredients;
        this.stores = stores;
        this.product_name = product_name;
        this.nutrient_levels = nutrient_levels;
        this.nutriments = nutriments;
        this.quantity = quantity;
        this.ingredients_text = ingredients_text;
        this.image_url = image_url;
        this.traces_tags = traces_tags;
        this.vitamins_tags = vitamins_tags;
        this.categories = categories;
        this.origins = origins;
        this.code = code;
        this.allergens = allergens;
        this.nutrition_grades = nutrition_grades;
        this.ingredients_text_with_allergens = ingredients_text_with_allergens;
        this.labels = labels;
        this.brands = brands;
        this.allergens_tags = allergens_tags;
        this.traces = traces;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public List<Map> getIngredients() {
        return ingredients;
    }

    public void setIngredients(List<Map> ingredients) {
        this.ingredients = ingredients;
    }

    public String getStores() {
        return stores;
    }

    public void setStores(String stores) {
        this.stores = stores;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public Map<String, Object> getNutrient_levels() {
        return nutrient_levels;
    }

    public void setNutrient_levels(Map<String, Object> nutrient_levels) {
        this.nutrient_levels = nutrient_levels;
    }

    public Map<String, Object> getNutriments() {
        return nutriments;
    }

    public void setNutriments(Map<String, Object> nutriments) {
        this.nutriments = nutriments;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getIngredients_text() {
        return ingredients_text;
    }

    public void setIngredients_text(String ingredients_text) {
        this.ingredients_text = ingredients_text;
    }


    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public List<String> getTraces_tags() {
        return traces_tags;
    }

    public void setTraces_tags(List<String> traces_tags) {
        this.traces_tags = traces_tags;
    }

    public List<String> getVitamins_tags() {
        return vitamins_tags;
    }

    public void setVitamins_tags(List<String> vitamins_tags) {
        this.vitamins_tags = vitamins_tags;
    }

    public String getCategories() {
        return categories;
    }

    public void setCategories(String categories) {
        this.categories = categories;
    }

    public String getOrigins() {
        return origins;
    }

    public void setOrigins(String origins) {
        this.origins = origins;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getAllergens() {
        return allergens;
    }

    public void setAllergens(String allergens) {
        this.allergens = allergens;
    }

    public String getNutrition_grades() {
        return nutrition_grades;
    }

    public void setNutrition_grades(String nutrition_grades) {
        this.nutrition_grades = nutrition_grades;
    }

    public String getIngredients_text_with_allergens() {
        return ingredients_text_with_allergens;
    }

    public void setIngredients_text_with_allergens(String ingredients_text_with_allergens) {
        this.ingredients_text_with_allergens = ingredients_text_with_allergens;
    }

    public String getLabels() {
        return labels;
    }

    public void setLabels(String labels) {
        this.labels = labels;
    }

    public String getBrands() {
        return brands;
    }

    public void setBrands(String brands) {
        this.brands = brands;
    }

    public List<String> getAllergens_tags() {
        return allergens_tags;
    }

    public void setAllergens_tags(List<String> allergens_tags) {
        this.allergens_tags = allergens_tags;
    }

    public String getTraces() {
        return traces;
    }

    public void setTraces(String traces) {
        this.traces = traces;
    }

    @Override
    public String toString() {
        return "Product{" +
                "_id='" + _id + '\'' +
                ", ingredients=" + ingredients +
                ", stores='" + stores + '\'' +
                ", product_name='" + product_name + '\'' +
                ", nutrient_levels=" + nutrient_levels +
                ", nutriments=" + nutriments +
                ", quantity='" + quantity + '\'' +
                ", ingredients_text='" + ingredients_text + '\'' +
                ", image_url='" + image_url + '\'' +
                ", traces_tags=" + traces_tags +
                ", vitamins_tags=" + vitamins_tags +
                ", categories='" + categories + '\'' +
                ", origins='" + origins + '\'' +
                ", code='" + code + '\'' +
                ", allergens='" + allergens + '\'' +
                ", nutrition_grades='" + nutrition_grades + '\'' +
                ", ingredients_text_with_allergens='" + ingredients_text_with_allergens + '\'' +
                ", labels='" + labels + '\'' +
                ", brands='" + brands + '\'' +
                ", allergens_tags=" + allergens_tags +
                ", traces='" + traces + '\'' +
                '}';
    }

    @Override
    public Product read() throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException {
        return null;
    }

}
