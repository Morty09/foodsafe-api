package com.esgi.foodsafeapi.models;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;
import java.util.List;

@Document(collection = "users")

public class User {

        @Id
        String _id;

        String firstname;
        String lastname;
        String email;
        String password;
        List<String> list_allergen;
        List<List<Product>> list_shop_history;
        List<Product> list_shop;
        List<String> list_product_history;

        public User() {
        }

        public User(String id, String firstname, String lastname) {
            this._id = id;
            this.firstname = firstname;
            this.lastname = lastname;
        }

        public User(String id, String firstname, String lastname, String email, String password, List<String> list_allergen,
                    List<List<Product>> list_shop_history, List<Product> list_shop, List<String> list_product_history)
        {
            this._id = id;
            this.firstname = firstname;
            this.lastname = lastname;
            this.email = email;
            this.password = password;
            this.list_allergen = list_allergen;
            this.list_shop_history = list_shop_history;
            this.list_shop = list_shop;
            this.list_product_history = list_product_history;
        }

        public String getId() {
            return _id;
        }

        public void setId(String id) {
            this._id = id;
        }

        public String getFirstname() {
            return firstname;
        }

        public void setFirstname(String firstname) {
            this.firstname = firstname;
        }

        public String getLastname() {
            return lastname;
        }

        public void setLastname(String lastname) {
            this.lastname = lastname;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public List<String> getList_allergen() {
            return list_allergen;
        }

        public void setList_allergen(List<String> list_allergen) {
            this.list_allergen = list_allergen;
        }

        public List<List<Product>> getList_shop_history() {
            return list_shop_history;
        }

        public void setList_shop_history(List<List<Product>> list_shop_history) {
            this.list_shop_history = list_shop_history;
        }

        public List<Product> getList_shop() {
            return list_shop;
        }

        public void setList_shop(List<Product> list_shop) {
            this.list_shop = list_shop;
        }

        public List<String> getList_product_history() {
            return list_product_history;
        }

        public void setList_product_history(List<String> list_product_history) {
            this.list_product_history = list_product_history;
        }

    @Override
    public String toString() {
        return "User{" +
                "_id='" + _id + '\'' +
                ", firstname='" + firstname + '\'' +
                ", lastname='" + lastname + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", list_allergen=" + list_allergen +
                ", list_shop_history=" + list_shop_history +
                ", list_shop=" + list_shop +
                ", list_product_history=" + list_product_history +
                '}';
    }
}
