package com.esgi.foodsafeapi.Service;

import com.esgi.foodsafeapi.interfaces.ProductRepository;
import com.esgi.foodsafeapi.interfaces.UserRepository;
import com.esgi.foodsafeapi.models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class UserService {

    @Autowired
    UserRepository userRepository;

    public User post(User user) {
        return userRepository.save(user);
    }

    public List<User> getAll() {
        return userRepository.findAll();
    }

    public Optional<User> getById(String id) {
        return userRepository.findById(id);
    }

    public Optional<List<User>> getByLastName(String lastname) {
        return userRepository.findByLastname(lastname);
    }

    public Optional<User> getByEmail(String email){
        return userRepository.findByEmail(email);
    }

    public Optional<User> update(String id, User user) {
        return userRepository.findById(id).map(newUser -> {
            if(user.getFirstname() != null)
                newUser.setFirstname(user.getFirstname());
            if(user.getLastname() != null)
                newUser.setLastname(user.getLastname());
            if(user.getEmail() != null)
                newUser.setEmail(user.getEmail());
            if(user.getPassword() != null)
                newUser.setPassword(user.getPassword());
            if(user.getList_allergen() != null)
                newUser.setList_allergen(user.getList_allergen());
            if (user.getList_shop_history() != null)
                newUser.setList_shop_history(user.getList_shop_history());
            if (user.getList_shop() != null)
                newUser.setList_shop(user.getList_shop());
            if (user.getList_product_history() != null)
                newUser.setList_product_history(user.getList_product_history());
            userRepository.save(newUser);
            return newUser;
        });
    }

    public void delete(User user) {
        userRepository.delete(user);
    }

}
