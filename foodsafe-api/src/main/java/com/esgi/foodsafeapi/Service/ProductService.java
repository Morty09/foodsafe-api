package com.esgi.foodsafeapi.Service;

import com.esgi.foodsafeapi.interfaces.ProductRepository;
import com.esgi.foodsafeapi.models.Ingredient;
import com.esgi.foodsafeapi.models.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class ProductService {

    @Autowired
    ProductRepository productRepository;

    public Product post(Product product) {
        return productRepository.save(product);
    }

    public List<Product> getAll() {
        return productRepository.findAll();
    }

    public Optional<Product> getByCode(String code) {
        return productRepository.findByCode(code);
    }

    public Optional<Product> getById(String id) {
        return productRepository.findById(id);
    }

    public Optional<Product> update(String code, Product product) {
        return productRepository.findByCode(code).map(newProduct -> {
            if (product.ingredients != null)
                newProduct.setIngredients(product.ingredients);
            if (product.stores != null)
                newProduct.setStores(product.stores);
            if (product.product_name != null)
                newProduct.setProduct_name(product.product_name);
            if (product.nutrient_levels != null)
                newProduct.setNutrient_levels(product.nutrient_levels);
            if (product.nutriments != null)
                newProduct.setNutrient_levels(product.nutriments);
            if (product.quantity != null)
                newProduct.setQuantity(product.quantity);
            if (product.ingredients_text != null)
                newProduct.setIngredients_text(product.ingredients_text);
            if (product.image_url != null)
                newProduct.setImage_url(product.image_url);
            if (product.traces_tags != null)
                newProduct.setTraces_tags(product.traces_tags);
            if (product.vitamins_tags != null)
                newProduct.setVitamins_tags(product.vitamins_tags);
            if (product.categories != null)
                newProduct.setCategories(product.categories);
            if (product.origins != null)
                newProduct.setOrigins(product.origins);
            if (product.code != null)
                newProduct.setCode(product.code);
            if (product.allergens != null)
                newProduct.setAllergens(product.allergens);
            if (product.nutrition_grades != null)
                newProduct.setNutrition_grades(product.nutrition_grades);
            if (product.ingredients_text_with_allergens != null)
                newProduct.setIngredients_text_with_allergens(product.ingredients_text_with_allergens);
            if (product.labels != null)
                newProduct.setLabels(product.labels);
            if (product.brands != null)
                newProduct.setBrands(product.brands);
            if (product.allergens_tags != null)
                newProduct.setAllergens_tags(product.allergens_tags);
            if (product.traces != null)
                newProduct.setTraces(product.traces);
            productRepository.save(newProduct);
            return newProduct;
        });
    }

    public void delete(Product product) {
        productRepository.delete(product);
    }

}
