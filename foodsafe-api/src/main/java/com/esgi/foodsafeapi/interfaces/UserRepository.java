package com.esgi.foodsafeapi.interfaces;

import com.esgi.foodsafeapi.models.User;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import javax.xml.ws.Response;
import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends MongoRepository<User, String> {

    Optional<List<User>> findByLastname(String lastName);

    Optional<User> findByEmail(String email);
}
