package com.esgi.foodsafeapi.controllers;

import com.esgi.foodsafeapi.Exceptions.ResourceNotFoundException;
import com.esgi.foodsafeapi.Service.IngredientService;
import com.esgi.foodsafeapi.interfaces.IngredientsRepository;
import com.esgi.foodsafeapi.models.Ingredient;
import com.esgi.foodsafeapi.models.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class IngredientController {

    @Autowired
    IngredientService ingredientService;

    @Autowired
    IngredientsRepository ingredientsRepository;

    public IngredientController() {

    }

    @RequestMapping(method = RequestMethod.GET, value = "/ingredients")
    public ResponseEntity<List<Ingredient>> getAllIngredients() {
        return new ResponseEntity<>(ingredientService.getAll(), HttpStatus.FOUND);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/ingredients")
    public ResponseEntity<Object> postIngredient(@RequestBody Ingredient ingredient) {

        ingredientService.post(ingredient);
        return new ResponseEntity<>("Ingredient created successfully", HttpStatus.CREATED);

    }

    @RequestMapping(method = RequestMethod.GET, value = "/ingredients/{id}")
    public ResponseEntity<Ingredient> getIngredientById(@PathVariable String id) {
        Ingredient ingredient = ingredientService.getById(id).orElseThrow(() -> new ResourceNotFoundException("Ingredient", "id", id));
        return new ResponseEntity<>(ingredient, HttpStatus.FOUND);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/ingredients/name/{ingredient_name}")
    public ResponseEntity<Ingredient> getIngredientByName(@PathVariable String ingredient_name) {
        Ingredient ingredient = ingredientService.getByName(ingredient_name).orElseThrow(() -> new ResourceNotFoundException("Ingredient", "name", ingredient_name));
        return new ResponseEntity<>(ingredient, HttpStatus.FOUND);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/ingredients/allergens/{status}")
    public List<Ingredient> getAllIngredientsWhereAllergenIsTrue(@PathVariable Boolean status) {
        try {
            List<Ingredient> ingredients = ingredientsRepository.findByAllergenType(status);
            for (Ingredient ingredient : ingredients) {
                System.out.println(ingredient.toString());
                if (ingredient.allergenType != status) {
                    ingredients.remove(ingredient);
                }
            }
            return ingredients;
        } catch (Exception e) {
            throw e;
        }
    }

    @RequestMapping(method = RequestMethod.GET, value = "/ingredients/productsIds/{ingredient_name}")
    public ResponseEntity<List<String>> getAllProductsIdsByIngredient(@PathVariable String ingredient_name) {

        Ingredient ingredient = ingredientService.getByName(ingredient_name).orElseThrow(() -> new ResourceNotFoundException("Ingredient", "name", ingredient_name));
        List<String> productsIds = ingredient.getProductsIds();
        return new ResponseEntity<>(productsIds, HttpStatus.FOUND);

    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/ingredients/{id}")
    public ResponseEntity<String> deleteIngredient(@PathVariable String id) {

        Ingredient ingredientToDelete = ingredientsRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Ingredient", "id", id));
        ingredientService.delete(ingredientToDelete);
        return new ResponseEntity<>("Ingredient with id " + id + " was deleted successfully", HttpStatus.OK);

    }

    @RequestMapping(method = RequestMethod.PUT, value = "/ingredients/{id}")
    public ResponseEntity<Ingredient> updateIngredient(@PathVariable String id, @RequestBody Ingredient ingredient) {
        Ingredient newIngredient = ingredientService.update(id, ingredient).orElseThrow(() -> new ResourceNotFoundException("Ingredient", "id", id));
        return new ResponseEntity<>(newIngredient, HttpStatus.OK);
    }

}
